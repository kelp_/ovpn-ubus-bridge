#ifndef SOCKET_H
#define SOCKET_H

#include "config.h"

#define MANAGEMENT_IP_ADDR "0.0.0.0"

void init_management_sockets(struct ovpn_management_info values[MAX_OVPN_INSTANCES]);

void close_management_sockets(struct ovpn_management_info values[MAX_OVPN_INSTANCES]);

#endif /* SOCKET_H */