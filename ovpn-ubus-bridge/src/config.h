#ifndef CONFIG_H
#define CONFIG_H

#include <netinet/in.h>


#define MAX_OVPN_INSTANCES 50 // low memory footprint, we can go beyond machine limits on this.

// hard limit for amount of commandline arguments from procd.
// if you seem to be getting random failures to read config values, try increasing this 
#define OPENVPN_MAX_ARG_COUNT 50 

// simple struct to map every openvpn config instance
// to a port.
struct ovpn_management_info {
    char *instance_name;
    int port;
    int sockfd; // set during socket init
    struct sockaddr_in sockaddr; // set during socket init
};

// properly sets up the instance array in order to avoid undefined behaviour
void init_ovpn_instance_data(struct ovpn_management_info values[MAX_OVPN_INSTANCES]);

// function to read openvpn instances, their management ports.
// ports are randomly assigned (from dynamic TCP range) in openvpn's procd init script.
// this function gets the instacne name and digs through the cli args (provided by procd ubus call)
// to find the management port.
int read_ovpn_instances(struct ovpn_management_info values[MAX_OVPN_INSTANCES]);

// simple function to retreive a full config struct via it's instance name
struct ovpn_management_info get_config_from_instance_name(struct ovpn_management_info values[MAX_OVPN_INSTANCES], char *name);

// debug print all ovpn instances and their management ports
void debug_print_ovpn_instances(struct ovpn_management_info values[MAX_OVPN_INSTANCES]); 

// returns:
// 0 if not null
// 1 if null
int ovpn_instance_null_check(struct ovpn_management_info value);

#endif /* CONFIG_H */