#include <libubox/blobmsg_json.h>
#include <libubox/blobmsg.h>
#include <libubox/blob.h>
#include <libubus.h>
#include <ubusmsg.h>
#include <syslog.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>


#include "config.h"
#include "stdio.h"
#include "string.h"


#define CMD_BUFSIZ 2000

// config array, made only accessible to the ubus callbacks within this file.
// no other way i know of to do config access across callbacks that dont have priv void pointers.
// best i can do is limit the technical debt caused by a global variable by limiting it's accessibility
// NOTE: must be set and filled before ubus object is registered.
static struct ovpn_management_info local_conf_values[MAX_OVPN_INSTANCES];

// enum for holding all possible states
// of the status parser
enum {
    PARSER_STATE_NORMAL,
    PARSER_STATE_CLIENT,
    PARSER_STATE_ROUTING,
    PARSER_STATE_GLOBALS
};

// ovpn instance status call value definitions
enum {
    STATUS_INSTANCE,
    __STATUS_MAX
};

static const struct blobmsg_policy ovpn_status_policy[] = {
    [STATUS_INSTANCE] = { .name = "instance", .type = BLOBMSG_TYPE_STRING},
};



// main call function for ovpn instance status call
static int ovpn_status(struct ubus_context *ctx, struct ubus_object *obj,
                       struct ubus_request_data *req, const char *method,
                       struct blob_attr *msg) {
    // output buffer
    static struct blob_buf buf;

    // blob_buf cookies, used for opening/closing arrays and tables.
    void *cookie = NULL; // sub-level 1
    void *cookie2 = NULL; // sub-level 2

    // status output parsing related
    char *line_buf = NULL; // generic buffers
    char *line_buf2 = NULL;
    char *saveptr1, *saveptr2; // pointers used in strtok_r, also generic
    char *line;
    int parse_state; // current state of status output parser

    // input buffers
    struct blob_attr *data[__STATUS_MAX];
    char *instance_name = NULL;

    // socket-related
    char command_output[CMD_BUFSIZ] = {0};
    int bytes_read = 0;

    blobmsg_parse(ovpn_status_policy, __STATUS_MAX, data, blob_data(msg), blob_len(msg));

    if (!data[STATUS_INSTANCE]) {
        return UBUS_STATUS_INVALID_ARGUMENT;
    }

    instance_name = blobmsg_get_string(data[STATUS_INSTANCE]);

    struct ovpn_management_info instance_info = get_config_from_instance_name(local_conf_values, instance_name);

    if (instance_info.port == -1) {
        // no instance of right name was found
        return UBUS_STATUS_INVALID_ARGUMENT;
    }

    // send the command into openvpn
    char *command = "status\r\n";
    if(send(instance_info.sockfd, command, strlen(command) , 0) < 0) {
        syslog(LOG_ERR, "Failed to send command into openvpn");
        return UBUS_STATUS_UNKNOWN_ERROR;
    }
    
    
    // receive command values from openvpn
    while (1) { 
        bytes_read = recv(instance_info.sockfd, command_output, 2000, 0);
        if (bytes_read < 0) {
            syslog(LOG_ERR, "Failed to receive data from openvpn");
            return UBUS_STATUS_UNKNOWN_ERROR;
        }
        // sanity check that we do indeed have the right status output
        if (strncmp(command_output, "OpenVPN CLIENT LIST", strlen("OpenVPN CLIENT LIST")) == 0) {
            break;
        }
    }

    // initialize reply blobmsg
    blob_buf_init(&buf, 0);

    // curent state of parser
    // while state is NORMAL, look for specific values within the lines to indicate
    // an upcoming table.
    // while state is CLIENT, parse every line as if it described a connected openvpn client.
    // while state is ROUTING, parse every line as if it described a routing directive
    parse_state = PARSER_STATE_NORMAL;

    // initial line read of command output
    line = strtok_r(command_output, "\n", &saveptr1);

    while (line != NULL) {
        // hacky string comparisons, but there's no way that I know of around them.
        // i would put them in as #defines, but this is the only place where they 
        // are used.
        if (strncmp(line, "Common Name,", strlen("Common Name,")) == 0) { // line indicates that next lines will be client list
            parse_state = PARSER_STATE_CLIENT;
            cookie = blobmsg_open_array(&buf, "client_list");
            line = strtok_r(NULL, "\n", &saveptr1);
            continue;
            
        } else if (strncmp(line, "ROUTING TABLE", strlen("ROUTING TABLE")) == 0) { // line indicates end of client list
            if (parse_state == PARSER_STATE_CLIENT) {
                blobmsg_close_array(&buf, cookie);
            }
            parse_state = PARSER_STATE_NORMAL;
            line = strtok_r(NULL, "\n", &saveptr1);
            continue;

        } else if (strncmp(line, "Virtual Address,", strlen("Virtual Address,")) == 0) { // line indicates start of routing table
            parse_state = PARSER_STATE_ROUTING;
            cookie = blobmsg_open_array(&buf, "routing_table");
            line = strtok_r(NULL, "\n", &saveptr1);
            continue;
        } else if (strncmp(line, "GLOBAL STATS", strlen("GLOBAL STATS")) == 0) { // line indicates end of routing table and start of globals
 
            if (parse_state == PARSER_STATE_ROUTING) {
                blobmsg_close_array(&buf, cookie);
            }
            parse_state = PARSER_STATE_GLOBALS;
            cookie = blobmsg_open_table(&buf, "global_stats");
            line = strtok_r(NULL, "\n", &saveptr1);
            continue;
        } else if (strncmp(line, "END", strlen("END")) == 0 ) { // end of stats
            parse_state = PARSER_STATE_NORMAL;
            blobmsg_close_table(&buf, cookie);
            line = strtok_r(NULL, "\n", &saveptr1);
            continue;
        }

        if (parse_state == PARSER_STATE_CLIENT) {

            cookie2 = blobmsg_open_table(&buf, NULL);

            line_buf = strtok_r(line, ",", &saveptr2);
            blobmsg_add_string(&buf, "common_name", line_buf);

            line_buf = strtok_r(NULL, ",", &saveptr2);
            blobmsg_add_string(&buf, "real_addr", line_buf);

            line_buf = strtok_r(NULL, ",", &saveptr2);
            blobmsg_add_string(&buf, "bytes_recv", line_buf);

            line_buf = strtok_r(NULL, ",", &saveptr2);
            blobmsg_add_string(&buf, "bytes_sent", line_buf);
            
            line_buf = strtok_r(NULL, ",\r\n", &saveptr2);
            blobmsg_add_string(&buf, "connected_since", line_buf);

            blobmsg_close_table(&buf, cookie2);
        } else if (parse_state == PARSER_STATE_ROUTING) {
            cookie2 = blobmsg_open_table(&buf, NULL);

            line_buf = strtok_r(line, ",", &saveptr2);
            blobmsg_add_string(&buf, "virt_addr", line_buf);

            line_buf = strtok_r(NULL, ",", &saveptr2);
            blobmsg_add_string(&buf, "common_name", line_buf);

            line_buf = strtok_r(NULL, ",", &saveptr2);
            blobmsg_add_string(&buf, "real_addr", line_buf);

            line_buf = strtok_r(NULL, ",\r\n", &saveptr2);
            blobmsg_add_string(&buf, "last_ref", line_buf);

            blobmsg_close_table(&buf, cookie2);
        } else if (parse_state == PARSER_STATE_GLOBALS) {
            line_buf = strtok_r(line, ",", &saveptr2);
            line_buf2 = strdup(line_buf);
            line_buf = strtok_r(NULL, ",\r\n", &saveptr2);
            blobmsg_add_string(&buf, line_buf2, line_buf);
        }

        line = strtok_r(NULL, "\n", &saveptr1);
    }

    // JSON response example:
    /*
    {
        "client_list": [
            {
                "common_name": "asdfasdf",
                "real_addr": "192.168.158.15",
                "bytes_recv": "1635138",
                "bytes_sent": "1652163",
                "connected_since": "timedate"
            },

        ],
        "routing_table": [
            {
                "virtual_addr": "192.168.10.1",
                "real_addr": "73.165.15.21",
                "common_name": "asdfasdf",
                "last_ref": "timedate"
            },
            {
                "virtual_addr": "192.168.10.1",
                "real_addr": "73.165.15.21",
                "common_name": "asdfasdf",
                "last_ref": "timedate"
            },
            {
                "virtual_addr": "192.168.10.1",
                "real_addr": "73.165.15.21",
                "common_name": "asdfasdf",
                "last_ref": "timedate"
            },
        ],
        "global_stats": {
            "stat_name":"stat_value"
        },

    }
    */

    // send out blobmsg ubus reply
    ubus_send_reply(ctx, req, buf.head);
    return 0;
}



// ovpn instance revoke call value definitions
enum {
    REVOKE_INSTANCE,
    REVOKE_IPADDR,
    REVOKE_COMMON_NAME,
    __REVOKE_MAX
};

static const struct blobmsg_policy ovpn_revoke_policy[] = {
    [REVOKE_INSTANCE] = { .name = "instance", .type = BLOBMSG_TYPE_STRING},
    [REVOKE_IPADDR] = { .name = "ip_port", .type = BLOBMSG_TYPE_STRING},
    [REVOKE_COMMON_NAME] = { .name = "common_name", .type = BLOBMSG_TYPE_STRING},
};

// main call function for ovpn revoke call
static int ovpn_revoke(struct ubus_context *ctx, struct ubus_object *obj,
                       struct ubus_request_data *req, const char *method,
                       struct blob_attr *msg) {

    
    syslog(LOG_INFO, "t0");

    // steps:
    // get config from instance arg
    // get ipaddr from addr arg
    // send out message to sock

    struct blob_attr *data[__REVOKE_MAX];
    char *instance_name = NULL;
    char *killarg = NULL;
    char command[50]; // msg to send

    blobmsg_parse(ovpn_revoke_policy, __REVOKE_MAX, data, blob_data(msg), blob_len(msg));

    if (!data[REVOKE_INSTANCE]) {
        return UBUS_STATUS_INVALID_ARGUMENT;
    }

    instance_name = blobmsg_get_string(data[REVOKE_INSTANCE]);

    syslog(LOG_DEBUG, "t1");

    if (data[REVOKE_COMMON_NAME]) {
        killarg = blobmsg_get_string(data[REVOKE_COMMON_NAME]);
    }


    syslog(LOG_DEBUG, "t2");

    if (data[REVOKE_IPADDR]) {
        killarg = blobmsg_get_string(data[REVOKE_IPADDR]);
    }


    syslog(LOG_DEBUG, "t3");

    if (killarg == NULL) {
        return UBUS_STATUS_INVALID_ARGUMENT;
    }


    syslog(LOG_DEBUG, "t4");

    struct ovpn_management_info instance_info = get_config_from_instance_name(local_conf_values, instance_name);

    if (instance_info.port == -1) {
        // no instance of right name was found
        return UBUS_STATUS_INVALID_ARGUMENT;
    }

    syslog(LOG_INFO, "sending: kill %s", killarg);

    // send the command into openvpn
    snprintf(command, 50, "kill %s\r\n", killarg);

    syslog(LOG_DEBUG, "t5 %s", command);

    if(send(instance_info.sockfd, command, strlen(command) , 0) < 0) {
        syslog(LOG_ERR, "Failed to send command into openvpn");
        return UBUS_STATUS_UNKNOWN_ERROR;
    }

    return 0;
}

// definitions of methods that this daemon provides
static const struct ubus_method ovpn_methods[] = {
    UBUS_METHOD("status", ovpn_status, ovpn_status_policy),
    UBUS_METHOD("revoke", ovpn_revoke, ovpn_revoke_policy)
};


// object definitions for all ubus calls
static struct ubus_object_type ovpn_object_type =
    UBUS_OBJECT_TYPE("openvpn", ovpn_methods);

static struct ubus_object ovpn_object = {
         .name = "openvpn",
         .type = &ovpn_object_type,
         .methods = ovpn_methods,
         .n_methods = ARRAY_SIZE(ovpn_methods),
};


struct ubus_context *init_ubus(struct ovpn_management_info values[MAX_OVPN_INSTANCES]) {

    // initialize local conf
    init_ovpn_instance_data(local_conf_values);

    // copy over config values from config in main scope into local scope
    for (int i = 0; i < MAX_OVPN_INSTANCES; i++) {
        local_conf_values[i] = values[i];
        if (values[i].instance_name != NULL) {
            local_conf_values[i] = values[i];
            local_conf_values[i].instance_name = strdup(values[i].instance_name);
            // TODO: check if other values are good
        }
    }

    struct ubus_context *ctx;
    int rc;

    uloop_init();
    ctx = ubus_connect(NULL);

    if (!ctx) {
        syslog(LOG_ERR, "Failed to connect to ubus.");
    }

    ubus_add_uloop(ctx);
    ubus_add_object(ctx, &ovpn_object);
    return ctx;
}

