#include <libubox/blobmsg_json.h>
#include <libubox/blobmsg.h>
#include <libubox/blob.h>
#include <libubus.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>

#include "config.h"

static void command_cb(struct ubus_request *req, int type, struct blob_attr *msg) {

    struct ovpn_management_info *values = req->priv;

    int val_index = 0;

    // str array to store arguments passed to openvpn
    char *args[OPENVPN_MAX_ARG_COUNT]; 
    int arg_index = 0;

    struct blob_attr *cur, *cur2, *cur3, *cur4, *cur5;
    int rem, rem2, rem3, rem4, rem5;

    // forced to build code pyramids here.
    // i've checked the sources of other projects utilizing ubus
    // most of them implement a similar method of parsing.

    blobmsg_for_each_attr(cur, msg, rem) { // "openvpn": {
        if (strcmp(blobmsg_name(cur), "openvpn") != 0) {
            continue;
        }
        blobmsg_for_each_attr(cur2, cur, rem2) { // "instances": {
            if (strcmp(blobmsg_name(cur2), "instances") != 0) {
                continue;
            }
            blobmsg_for_each_attr(cur3, cur2, rem3) { // "[instance_name]": {
                // set instance name
                values[val_index].instance_name = blobmsg_name(cur3);

                // read all arguments here
                blobmsg_for_each_attr(cur4, cur3, rem4) { // "command": [
                    if (blobmsg_type(cur4) != BLOBMSG_TYPE_ARRAY) {
                        continue;
                    }
                    
                    if (strcmp(blobmsg_name(cur4), "command") != 0) {
                        continue;
                    }

                    // parse port
                    blobmsg_for_each_attr(cur5, cur4, rem5) { // values inside "command"
                        args[arg_index] = blobmsg_get_string(cur5);

                        // management port is second argument for --management flag
                        if (arg_index < 2) { // 
                            arg_index++;
                            continue;
                        }

                        // check if argument 2 args before this one is the management flag
                        if (strcmp(args[arg_index-2], "--management") == 0) {
                            // if true, we now know that:
                            // args[arg_index-1] is management ip addr
                            // args[arg_index] is management port
                            values[val_index].port = atoi(args[arg_index]); // convert and set port
                        }
                        arg_index++;
                    } // values inside "command"
                } // "command"
                val_index++; 
            } // [instance_name]
        } // "instances"
    } // "openvpn"

    //syslog(LOG_DEBUG, "printing values inside parsing callback:");
    //debug_print_ovpn_instances(values);
}

void init_ovpn_instance_data(struct ovpn_management_info values[MAX_OVPN_INSTANCES]) {
    for (int i = 0; i < MAX_OVPN_INSTANCES; i++) {
        values[i].instance_name = NULL; // set to nullptr
        values[i].port = -1; // set to invalid port
        values[i].sockfd = -1; // set to invalid sockfd
        // sockaddr will be set later
    }
}

// returns:
// 0 on successful read
// -1 on ubus/procd error
int read_ovpn_instances(struct ovpn_management_info values[MAX_OVPN_INSTANCES]) {
    struct ubus_context *ctx;
    struct blob_buf b = { };
    uint32_t id;

    struct ovpn_management_info *arrpointer = values;

    ctx = ubus_connect(NULL);
    if (!ctx) {
        syslog(LOG_ERR, "Failed to connect to ubus.");
        return -1;
    }

    blob_buf_init(&b, 0);
    blobmsg_add_string(&b, "name", "openvpn");

    if (ubus_lookup_id(ctx, "service", &id) || 
        ubus_invoke(ctx, id, "list", b.head, command_cb, arrpointer, 3000)) {
        syslog(LOG_ERR, "Cannot request service info from procd.");
        return -1;
    }

    ubus_free(ctx);
    return 0;
}

struct ovpn_management_info get_config_from_instance_name(struct ovpn_management_info values[MAX_OVPN_INSTANCES], char *name) {
    for (int i = 0; i < MAX_OVPN_INSTANCES; i++) {
        if (ovpn_instance_null_check(values[i]) == 1) {
            break;
        }

        if (strcmp(values[i].instance_name, name) == 0) {
            return values[i];
        }
    }

    // too hacky
    // TODO: rewrite this
    struct ovpn_management_info null_output;
    null_output.port = -1;

    return null_output;
}


// returns:
// 0 if not null
// 1 if null
int ovpn_instance_null_check(struct ovpn_management_info value) {
    if (!value.instance_name || value.port <= 0) {
        return 1;
    }
    return 0;
}

void debug_print_ovpn_instances(struct ovpn_management_info values[MAX_OVPN_INSTANCES]) {
    for (int i = 0; i < MAX_OVPN_INSTANCES; i++) {
        
        if (ovpn_instance_null_check(values[i]) == 1) {
          syslog(LOG_DEBUG, "value at %d is null, breaking.", i);
          break;
        }
        syslog(LOG_DEBUG, "instance: %s, port: %d\n", values[i].instance_name, values[i].port);
    }
}