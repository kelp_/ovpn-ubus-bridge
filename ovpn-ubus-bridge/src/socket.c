#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/socket.h>


#include "socket.h"
#include "config.h"


void init_management_sockets(struct ovpn_management_info values[MAX_OVPN_INSTANCES]) {

    for (int i = 0; i < MAX_OVPN_INSTANCES; i++) {

        if (ovpn_instance_null_check(values[i]) == 1) {
            break;
        }

        values[i].sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (values[i].sockfd == -1) {
            syslog(LOG_ERR, "failed to open management socket for %s", values[i].instance_name);
            continue;
        }
        
        syslog(LOG_INFO, "Socket creation for %s successful", values[i].instance_name);

        bzero(&(values[i].sockaddr), sizeof(values[i].sockaddr));
    
        // assign IP, PORT
        values[i].sockaddr.sin_family = AF_INET;
        values[i].sockaddr.sin_addr.s_addr = inet_addr(MANAGEMENT_IP_ADDR);
        values[i].sockaddr.sin_port = htons(values[i].port);
    
        // connect the client socket to server socket
        if (connect(values[i].sockfd, (struct sockaddr*)&(values[i].sockaddr), sizeof(values[i].sockaddr)) != 0) {
            syslog(LOG_INFO, "failed to connect to management socket for %s", values[i].instance_name);
            continue;
        }

        syslog(LOG_INFO, "Managment socket connection for %s successful", values[i].instance_name);

    }

}

void close_management_sockets(struct ovpn_management_info values[MAX_OVPN_INSTANCES]) {

    for (int i = 0; i < MAX_OVPN_INSTANCES; i++) {
        if (ovpn_instance_null_check(values[i]) == 1) {
            break;
        }
        close(values[i].sockfd);
    }
}