#ifndef UBUS_CALLS_H
#define UBUS_CALLS_H

#include <libubus.h>

#include "config.h"

//https://lxr.openwrt.org/source/ubus/examples/server.c
// single function that inits ubus, adds methods, etcetc
// returns full ubus context, to allow starting of uloop in main scope
struct ubus_context *init_ubus(struct ovpn_management_info values[MAX_OVPN_INSTANCES]);


#endif /* UBUS_CALLS_H */