#include <stdio.h>
#include <signal.h>
#include <syslog.h>
#include <unistd.h>
#include <stdlib.h>
#include <libubox/uloop.h>

#include "config.h"

#include "socket.h"
#include "ubus_calls.h"

volatile sig_atomic_t deamonize = 1;

int main() {
    int rc;
    struct ovpn_management_info conf_values[MAX_OVPN_INSTANCES];
    struct ubus_context *main_ctx;

    // init log
    openlog("ovpn_ubus_bridge", LOG_INFO, 0);
    syslog(LOG_INFO, "Initializing");

    init_ovpn_instance_data(conf_values);

    // read config
    rc = read_ovpn_instances(conf_values);
    if (rc != 0) {
        syslog(LOG_INFO, "Config read error (rc: %d).", rc);
        return 1; // could not read config
    }

    syslog(LOG_INFO, "Config read successful.");
    //debug_print_ovpn_instances(conf_values);

    // initialize TCP sockets
    init_management_sockets(conf_values);

    // init ubus
    syslog(LOG_INFO, "Initializing ubus listeners.");
    main_ctx = init_ubus(conf_values);

    // start main loop
    syslog(LOG_INFO, "Initialization complete. Listening.");
    uloop_run();
    syslog(LOG_INFO, "Stopping.");

    close_management_sockets(conf_values);
    closelog();
    return 0;
}
