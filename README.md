# ovpn-ubus-bridge

## About

This is a simple daemon to link together ubus and openvpn.
Main communication is done over TCP sockets and openvpn's management interface.

## Compile and Install

**Compilation requires a working openwrt development toolchain and enviroment**

* `git clone` this repository
* link or copy over the ovpn-ubus-folder into `/package/`
* copy over the openvpn init file from this repository's `openvpn` folder into `/package/network/services/openvpn/files`
* run `make menuconfig`, then, inside the menu, enable the `ovpn-ubus-bridge` package, and enable the management interface flag for the `openvpn` package (enable `openvpn` too if it's not enabled).
* `make package/network/services/openvpn/compile`
* `make package/ovpn-ubus-bridge`
* `scp` over the newly built .ipk files and install via `opkg`


## ubus methods

| Path    | Procedure | Signature                                                            | Description                                                                                                                |
|---------|-----------|----------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------|
| openvpn | status    | {"instance": "String"}                                               | Get the status of a running openvpn instance, here you can find common names and IP addresses for all clients of instance. |
| openvpn | revoke    | {"instance": "String", "ip_port": "String", "common_name": "String"} | revoke/disconnect an openvpn client from an instance, ONLY ONE ARGUMENT OF `ip_port` OR `common_name` is required          |

## How does it work

During the service init of openvpn, a random management port (from the dynamic IP port range) is added to the command line arguments of openvpn. This program retreives said port via ubus call to procd, links it with the instance name, and uses it to provide additional
ubus calls for every openvpn instance running on the system.

